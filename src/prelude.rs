use cgmath::Vector3;

pub const WHITE_CLR: Vector = Vector::new(1.0, 1.0, 1.0);
pub const SKY_CLR: Vector = Vector::new(0.5, 0.7, 1.0);
pub type Vector = Vector3<f32>;
