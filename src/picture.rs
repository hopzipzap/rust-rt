use crate::color::Color;

pub struct Picture {
	w: usize,
	h: usize,
	colors: Vec<Color>,
}

impl Picture {
	pub fn new(w: usize, h: usize) -> Self {
		Picture {
			w,
			h,
			colors: vec![Color { r: 0, g: 0, b: 0 }; w * h],
		}
	}
	pub fn mutate<F>(&mut self, mutor: F)
	where
		F: FnOnce(&mut [Color], usize, usize),
	{
		mutor(&mut self.colors, self.w, self.h);
	}
	pub fn print_to_ppm(self) {
		println!("P3");
		println!("{} {}", self.w, self.h);
		println!("255");

		for j in (0..self.h).rev() {
			for i in 0..self.w {
				print!("{}", self.colors[j * self.w + i].color_to_string());
			}
			println!();
		}
	}
}
