use crate::Vector;

#[derive(Clone)]
pub struct Ray {
	pub origin: Vector,
	pub direction: Vector,
}

impl Ray {
	pub fn at(&self, t: f32) -> Vector {
		self.origin + self.direction * t
	}
}
