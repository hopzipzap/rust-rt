mod color;
mod picture;
mod prelude;
mod ray;

use {
	cgmath::{prelude::*, Vector3},
	color::Color,
	picture::Picture,
	prelude::*,
	ray::Ray,
};

fn test_sphere(ray: &Ray, sphere: (Vector, f32)) -> Option<(Vector, Vector)> {
	let (center, radius) = sphere;
	let so = ray.origin - center;
	let a = ray.direction.dot(ray.direction);
	let b = 2.0 * so.dot(ray.direction);
	let c = so.dot(so) - radius * radius;
	let dsc = b * b - 4.0 * a * c;
	if dsc.abs() < 0.000001 {
		let x = -b / (2.0 * a);
		let p = ray.at(x);
		let n = (p - center).normalize();
		// println!("Dont fuck me");
		Some((p, n))
	} else if dsc < 0.0 {
		// println!("Fuck me");
		None
	} else {
		let (x0, x1) = (-b + dsc.sqrt() / (2.0 * a), b + dsc.sqrt() / (2.0 * a));
		let (p0, p1) = (ray.at(x0), ray.at(x1));
		let p = if (p0 - ray.origin).dot(p0 - ray.origin) < (p1 - ray.origin).dot(p1 - ray.origin) {
			p0
		} else {
			p1
		};

		let n = (p - center).normalize();
		// println!("Oh hell yeah");
		Some((p, n))
	}
}

fn sky_color(ray: &Ray) -> Vector {
	let dir_norm = ray.direction.normalize();
	let y_rescale = (dir_norm.y + 1.0) / 2.0;
	WHITE_CLR.lerp(SKY_CLR, y_rescale)
}

fn main() {
	let mut pic = Picture::new(800, 800);

	pic.mutate(|colors, w, h| {
		// DRAW SOME SHIT
		for j in 0..h {
			for i in 0..w {
				let dir = Vector3::new(
					i as f32 / w as f32 * 2.0 - 1.0,
					j as f32 / h as f32 * 2.0 - 1.0,
					1.0,
				);
				let ray = Ray {
					origin: Vector3::zero(),
					direction: dir,
				};
				let sky_color = sky_color(&ray);
				let radius = 0.5;
				let pos = Vector3::new(0.0, 0.0, -1.0);
				let sphere = (pos, radius);
				// let image = if let Some((p,n)) = test_sphere(&ray, sphere){

				// }
				let image;
				match test_sphere(&ray, sphere) {
					Some(_) => image = Vector::new(1.0, 0.0, 0.0),
					None => image = sky_color,
				}
				colors[i + j * w] = Color::from(image * 255.99);
			}
		}
	});

	pic.print_to_ppm();
}
